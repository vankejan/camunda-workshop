package cz.cvut.fel.camunda.workshops.developer.handlers;

import lombok.Data;

@Data
public class UserDetail {
    private String username;
    private String name;
}
