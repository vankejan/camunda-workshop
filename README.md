# Camunda DEV Workshop

To run the preconfigured instance of Camunda (and Postgres DB) use the following commands:


```
git clone git@gitlab.fel.cvut.cz:vankejan/camunda-workshop.git
cd camunda-workshop
docker compose up -d
```